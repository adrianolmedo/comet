<?php
// si la peticion es ajax
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { 

  require_once('conect.php');

  $fecha_ac = isset($_GET['timestamp']) ? $_GET['timestamp'] : 0;
  $fecha_bd = @$row['timestamp']; // lectura adelantada: extrae la ultima fecha de modificacion

  while ($fecha_bd <= $fecha_ac) { // comprobar si ha habido cambios entre las fechas
    usleep(10000); // dormir 10 ms para descargar la CPU
    clearstatcache();
    $query1   = "SELECT timestamp FROM mensajes ORDER BY timestamp DESC LIMIT 1";
    $result   = $mysqli->query($query1);
    $ro       = $result->fetch_array(MYSQLI_ASSOC);
    $fecha_bd = strtotime($ro['timestamp']);// si la fecha/hora de modificacion es diferente a la del registro, se rompe el ciclo y avanza despues del while
  }

  $query2 = "SELECT * FROM mensajes ORDER BY timestamp DESC LIMIT 1";
  $datos_query = $mysqli->query($query2);
  while ($row = $datos_query->fetch_array(MYSQLI_ASSOC)) {
    $response["timestamp"] = strtotime($row['timestamp']);  
    $response["mensaje"]   = $row['mensaje']; 
    $response["id"]        = $row['id'];  
  }

  echo json_encode($response);
  flush(); // Vaciar el búfer de salida del sistema
  $result->free();
  $datos_query->free();
  $mysqli->close();

}
?>