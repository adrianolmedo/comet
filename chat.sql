CREATE TABLE IF NOT EXISTS `mensajes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mensaje` varchar(100) NOT NULL,
  `timestamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf32 AUTO_INCREMENT=17 ;

INSERT INTO `mensajes` (`id`, `mensaje`, `timestamp`) VALUES
(1, 'Hola mundo', '2012-08-29 02:12:23'),
(6, 'adios mundo cruel', '2012-08-30 02:10:06'),
(7, 'Esta es una prueba de httpush', '2012-08-30 02:12:51'),
(8, 'otro mensaje', '2012-08-30 03:25:57'),
(9, '', '2016-07-07 04:22:17'),
(10, 'Division 1', '2016-07-07 04:24:41'),
(11, 'sdsdasd', '2016-07-07 04:24:58'),
(12, 'Division 1', '2016-07-07 04:25:09'),
(13, 'Divison 2', '2016-07-07 04:25:21'),
(14, 'Division 1', '2016-07-07 04:25:41'),
(15, 'Division 1300', '2016-07-07 04:25:50'),
(16, 'sdsdasdfafdfdffsfdfasd', '2016-07-07 04:26:08');