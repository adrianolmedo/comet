<?php
// si la peticion es ajax
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { 
	require_once('conect.php');

	// si detecta que el mensaje esta vacio
	if (!$_POST['mensaje']) {
		$data = array('res' => 'error', 'alert' => 'Escriba un mensaje.');
		echo json_encode($data);
	} else {
		// insercion de datos
		$mensaje 	= $_POST['mensaje'];
		$timestamp 	= date('Y-m-d H:i:s');
		$query 		= "INSERT INTO mensajes (mensaje, timestamp) VALUES ('$mensaje', '$timestamp')";
		$insert 	= $mysqli->query($query);

		// si la insercion de datos arroja verdadero entonces procede a enviar una respuesta json de exito 
		if ($insert == true) {
			$data = array('res' => 'success');
			echo json_encode($data);
		}
	}
}
?>